﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Cinemas.Models
{
    public partial class Movie
    {
        public Movie()
        {
            Tickets = new HashSet<Ticket>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int IdCategory { get; set; }
        public int IdActor { get; set; }

        public virtual Actor IdActorNavigation { get; set; }
        public virtual Category IdCategoryNavigation { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
