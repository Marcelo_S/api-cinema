﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Cinemas.Models
{
    public partial class Ticket
    {
        public int Id { get; set; }
        public int IdMovie { get; set; }
        public int IdSeat { get; set; }
        public int IdRoom { get; set; }
        public string Decription { get; set; }

        public virtual Movie IdMovieNavigation { get; set; }
        public virtual Room IdRoomNavigation { get; set; }
        public virtual Seat IdSeatNavigation { get; set; }
    }
}
