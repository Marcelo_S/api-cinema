﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Cinemas.Models
{
    public partial class Seat
    {
        public Seat()
        {
            RoomSeats = new HashSet<RoomSeat>();
            Tickets = new HashSet<Ticket>();
        }

        public int Id { get; set; }
        public int Number { get; set; }
        public byte State { get; set; }

        public virtual ICollection<RoomSeat> RoomSeats { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
