﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Cinemas.Models
{
    public partial class Actor
    {
        public Actor()
        {
            Movies = new HashSet<Movie>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
    }
}
