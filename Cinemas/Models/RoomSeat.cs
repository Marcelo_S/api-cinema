﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Cinemas.Models
{
    public partial class RoomSeat
    {
        public int Id { get; set; }
        public int IdSeat { get; set; }
        public int IdRoom { get; set; }

        public virtual Room IdRoomNavigation { get; set; }
        public virtual Seat IdSeatNavigation { get; set; }
    }
}
