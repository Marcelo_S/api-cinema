﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cinemas.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(maxLength: 100, nullable: false),
                    lastname = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    description = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Rooms",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    number = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rooms", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Seats",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    number = table.Column<int>(nullable: false),
                    state = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seats", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    id_category = table.Column<int>(nullable: false),
                    id_actor = table.Column<int>(nullable: false),
                    IdActorNavigationId = table.Column<int>(nullable: true),
                    IdCategoryNavigationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.id);
                    table.ForeignKey(
                        name: "FK_Movies_Actors_IdActorNavigationId",
                        column: x => x.IdActorNavigationId,
                        principalTable: "Actors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Movies_Categories_IdCategoryNavigationId",
                        column: x => x.IdCategoryNavigationId,
                        principalTable: "Categories",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Room_seat",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_seat = table.Column<int>(nullable: false),
                    id_room = table.Column<int>(nullable: false),
                    IdRoomNavigationId = table.Column<int>(nullable: true),
                    IdSeatNavigationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Room_seat", x => x.id);
                    table.ForeignKey(
                        name: "FK_Room_seat_Rooms_IdRoomNavigationId",
                        column: x => x.IdRoomNavigationId,
                        principalTable: "Rooms",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Room_seat_Seats_IdSeatNavigationId",
                        column: x => x.IdSeatNavigationId,
                        principalTable: "Seats",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tickets",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_movie = table.Column<int>(nullable: false),
                    id_seat = table.Column<int>(nullable: false),
                    id_room = table.Column<int>(nullable: false),
                    decription = table.Column<string>(maxLength: 100, nullable: true),
                    IdMovieNavigationId = table.Column<int>(nullable: true),
                    IdRoomNavigationId = table.Column<int>(nullable: true),
                    IdSeatNavigationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tickets", x => x.id);
                    table.ForeignKey(
                        name: "FK_Tickets_Movies_IdMovieNavigationId",
                        column: x => x.IdMovieNavigationId,
                        principalTable: "Movies",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tickets_Rooms_IdRoomNavigationId",
                        column: x => x.IdRoomNavigationId,
                        principalTable: "Rooms",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tickets_Seats_IdSeatNavigationId",
                        column: x => x.IdSeatNavigationId,
                        principalTable: "Seats",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movies_IdActorNavigationId",
                table: "Movies",
                column: "IdActorNavigationId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_IdCategoryNavigationId",
                table: "Movies",
                column: "IdCategoryNavigationId");

            migrationBuilder.CreateIndex(
                name: "IX_Room_seat_IdRoomNavigationId",
                table: "Room_seat",
                column: "IdRoomNavigationId");

            migrationBuilder.CreateIndex(
                name: "IX_Room_seat_IdSeatNavigationId",
                table: "Room_seat",
                column: "IdSeatNavigationId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_IdMovieNavigationId",
                table: "Tickets",
                column: "IdMovieNavigationId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_IdRoomNavigationId",
                table: "Tickets",
                column: "IdRoomNavigationId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_IdSeatNavigationId",
                table: "Tickets",
                column: "IdSeatNavigationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Room_seat");

            migrationBuilder.DropTable(
                name: "Tickets");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Rooms");

            migrationBuilder.DropTable(
                name: "Seats");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
