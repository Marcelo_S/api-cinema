﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cinemas.Models;

namespace Cinemas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomSeatsController : ControllerBase
    {
        private readonly sala_cineContext _context;

        public RoomSeatsController(sala_cineContext context)
        {
            _context = context;
        }

        // GET: api/RoomSeats
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoomSeat>>> GetRoomSeats()
        {
            return await _context.RoomSeats.ToListAsync();
        }

        // GET: api/RoomSeats/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RoomSeat>> GetRoomSeat(int id)
        {
            var roomSeat = await _context.RoomSeats.FindAsync(id);

            if (roomSeat == null)
            {
                return NotFound();
            }

            return roomSeat;
        }

        // PUT: api/RoomSeats/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRoomSeat(int id, RoomSeat roomSeat)
        {
            if (id != roomSeat.Id)
            {
                return BadRequest();
            }

            _context.Entry(roomSeat).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoomSeatExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RoomSeats
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<RoomSeat>> PostRoomSeat(RoomSeat roomSeat)
        {
            _context.RoomSeats.Add(roomSeat);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRoomSeat", new { id = roomSeat.Id }, roomSeat);
        }

        // DELETE: api/RoomSeats/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RoomSeat>> DeleteRoomSeat(int id)
        {
            var roomSeat = await _context.RoomSeats.FindAsync(id);
            if (roomSeat == null)
            {
                return NotFound();
            }

            _context.RoomSeats.Remove(roomSeat);
            await _context.SaveChangesAsync();

            return roomSeat;
        }

        private bool RoomSeatExists(int id)
        {
            return _context.RoomSeats.Any(e => e.Id == id);
        }
    }
}
